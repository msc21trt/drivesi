﻿CREATE FUNCTION empresa.total_horas(varchar(15)) RETURNS numeric AS $$
	SELECT sum(t.horas) AS total
	FROM empresa.trabalha_em as t, empresa.projeto as p
	WHERE p.pnumero = t.pno AND p.pjnome = $1
	$$ LANGUAGE SQL;

CREATE TYPE empresa.emp_sal AS (nome varchar(15), unome varchar(15), salario numeric);
CREATE FUNCTION empresa.empregado_horas(varchar(15)) RETURNS setof empresa.emp_sal AS $$
	SELECT e.fnome, e.lnome, t.horas
	FROM empresa.trabalha_em as t, empresa.empregado as e, empresa.projeto as p
	WHERE t.pno = p.pnumero AND t.essn = e.ssn AND p.pjnome = $1
	GROUP BY e.fnome, e.lnome, t.horas
	$$ LANGUAGE SQL;

-- TESTE
SELECT * FROM empresa.empregado_horas('ProdutoY');
SELECT * FROM empresa.total_horas('ProdutoY');

CREATE FUNCTION empresa.altera_localizacoes() RETURNS TRIGGER AS $gatilho$
	BEGIN
		IF tg_op = 'UPDATE' THEN UPDATE empresa.projeto
			SET plocalizacao = new.dlocalizacao
			WHERE plocalizacao = old.dlocalizacao;
			RETURN NEW;
		END IF;
		RETURN NULL;
	END;
	$gatilho$ LANGUAGE 'plpgsql';

CREATE TRIGGER gatilho
AFTER UPDATE ON empresa.dept_localizacoes.dlocalizacao
FOR EACH ROW
EXECUTE PROCEDURE empresa.altera_localizacoes();