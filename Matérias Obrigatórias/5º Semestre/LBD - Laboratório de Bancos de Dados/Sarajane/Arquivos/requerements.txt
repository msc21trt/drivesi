Requirement Specification

Example: Fists Steps (HELP DB-Main 9.1.1)

A library proposes copies of books to its borrowers.
Each book has a unique number (ISBN), a title and some authors (one to five).
Each copy of a book has a serial number that identifies it among all the copies of this book.
It also has a location (made up of a store, a shelf and a row).
A borrower is identified by his/her name and can have an address.

Must be a complete specification ...........


